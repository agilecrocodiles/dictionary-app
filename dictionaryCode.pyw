import tkinter as tk
import csv
from itertools import cycle

#create frame for the app in general
root = tk.Tk()
frame = tk.Frame(root)
frame.pack()

root.geometry('200x400')

#create database file if it doesn't already exist
file = open('database.csv', 'a')
file.close()

#define variables
global dictionary
global rowCount
global dictKeysCycle
global reverseDictKeysCycle

with open('database.csv', 'r') as read:
    csvRead = csv.reader(read)
    lines = list(read)
    rowCount = len(lines)
read.close()
dictionary = {}
global wordButton
wordButton = {}

#function to forget main screen
def forgetHome():
    addAWordButton.place_forget()
    for buttonKey in wordButton:
        wordButton[buttonKey].place_forget()
#function to draw the word adding screen
def openAddAWord():
    forgetHome()
    #add text
    labelWord.place(relx=0.5, y=15, anchor=tk.CENTER)
    
    nameEntryBox.place(relx=0.5, y=40, anchor=tk.CENTER)

    labelDef.place(relx=0.5, y=75, anchor=tk.CENTER)
    
    definition = ''
    defEntryBox.place(relx=0.5, y=225, anchor=tk.CENTER)

    saveButton.place(x=118, y=365)
    nameEntryBox.focus_set()
    closeAddWordButton.place(x=7, y=365)

# function to save the word to the dictionary
def saveAWord():
    aFile = open("database.csv", "a")
    csvFile = csv.writer(aFile)
    dictionary[nameEntryBox.get().capitalize()] = defEntryBox.get("1.0", "end")
    csvFile.writerow([str(nameEntryBox.get()).strip('\n').strip().capitalize(), str(defEntryBox.get("1.0", "end")).strip('\n').strip()])
    aFile.close()
    closeAddWord()

def closeAddWord():
    labelWord.place_forget()
    nameEntryBox.delete('0', 'end')
    nameEntryBox.place_forget()
    labelDef.place_forget()
    defEntryBox.delete('1.0', 'end')
    defEntryBox.place_forget()
    saveButton.place_forget()
    closeAddWordButton.place_forget()
    drawWords()

# function to draw the main screen
def drawWords():
    y = 65
    count=0
    keys = list(dictionary.keys())
    keys.sort()
    global dictKeysCycle
    dictKeysCycle = cycle(keys)
    global reversedDictKeysCycle
    reversedDictKeysCycle = cycle(reversed(keys))
    
    for item in keys:
        count+=1
        key = 'button' + str(count)
        wordButton[key] = tk.Button(root,
                              text = item,
                              fg = "navy blue",
                              bg = "#A5D9F3",
                              font="Helvetica 10",
                              command=lambda thisWord=item, thisDef=dictionary[item]: displayWord(thisWord, thisDef))
        wordButton[key].place(relx=.5, y=y, anchor=tk.CENTER)
        y+=33
    addAWordButton.place(relx=0.5, y=20, anchor=tk.CENTER)

def findNextWord():
    foundItem = False
    for item in dictKeysCycle:
        if (foundItem):
            global nextWord
            nextWord = item
            break
        if (item == currentWord):
            foundItem = True
    global nextDefinition
    nextDefinition = dictionary[nextWord]
    nextWordButton.place(x=105, y=335)

def findPrevWord():
    foundItem = False
    for item in reversedDictKeysCycle:
        if (foundItem):
            global prevWord
            prevWord = item
            break
        if (item == currentWord):
            foundItem = True
    global prevDefinition
    prevDefinition = dictionary[prevWord]
    prevWordButton.place(x=75, y=335)
    
def displayWord(word, definition):
    global currentWord
    currentWord = word
    global currentDefinition
    currentDefinition = definition
    findNextWord()
    findPrevWord()
    wordDefLabels['word'] = tk.Label(root, text=word, font="Helvetica 10", background="#449ECB", foreground="navy blue")
    wordDefLabels['def'] = tk.Label(root, text=definition, font="Helvetica 10", background="#449ECB", foreground="navy blue", wraplength=190, justify='left')
    
    forgetHome()
    labelWord.place(relx=.5, y=15, anchor=tk.CENTER)
    wordDefLabels['word'].place(x=0, y=30)
    labelDef.place(relx=.5, y=80, anchor=tk.CENTER)
    wordDefLabels['def'].place(x=0, y=95)
    closeWordPageButton.place(x=150, y=365)
    deleteButton.place(x=10, y=365)
    editButton.place(relx=.5, y=379, anchor=tk.CENTER)

def closeWordPage():
    labelWord.place_forget()
    labelDef.place_forget()
    nextWordButton.place_forget()
    prevWordButton.place_forget()
    wordDefLabels['word'].place_forget()
    wordDefLabels['def'].place_forget()
    closeWordPageButton.place_forget()
    deleteButton.place_forget()
    editButton.place_forget()
    drawWords()

def nextPrevWordPage(word, definition):
    closeWordPage()
    displayWord(word, definition)

def deleteWord(word):
    del dictionary[word]

    lines = list()
    with open('database.csv', 'r') as readFile:
        reader = csv.reader(readFile)
        for row in reader:
            lines.append(row)
            for field in row:
                if field == word:
                    lines.remove(row)
    with open('database.csv', 'w') as writeFile:
        writer = csv.writer(writeFile)
        writer.writerows(lines)
    
    closeWordPage()

def showEditPage():
    wordDefLabels['def'].place_forget()
    editButton.place_forget()
    closeWordPageButton.place_forget()
    nextWordButton.place_forget()
    prevWordButton.place_forget()
    defEntryBox.place(relx=0.5, y=225, anchor=tk.CENTER)
    defEntryBox.insert("1.0", currentDefinition)
    saveEditButton.place(relx=.5, y=379, anchor=tk.CENTER)
    cancelButton.place(x=140, y=365)

def closeEdit():
    defEntryBox.delete('1.0', 'end')
    saveEditButton.place_forget()
    cancelButton.place_forget()
    defEntryBox.place_forget()
    findNextWord()
    findPrevWord()
    editButton.place(relx=.5, y=379, anchor=tk.CENTER)
    wordDefLabels['def'].place(x=0, y=95)
    closeWordPageButton.place(x=150, y=365)

def saveEdit():
    #save dictionary
    currentDefinition = defEntryBox.get("1.0", "end")
    dictionary[currentWord] = currentDefinition

    #delete from database
    lines = list()
    with open('database.csv', 'r') as readFile:
        reader = csv.reader(readFile)
        for row in reader:
            lines.append(row)
            for field in row:
                if field == currentWord:
                    lines.remove(row)
    with open('database.csv', 'w') as writeFile:
        writer = csv.writer(writeFile)
        writer.writerows(lines)
    #add back to database
    aFile = open("database.csv", "a")
    csvFile = csv.writer(aFile)
    csvFile.writerow([currentWord, currentDefinition.strip('\n').strip()])
    aFile.close()

    wordDefLabels['def'] = tk.Label(root, text=currentDefinition, font="Helvetica 10", background="#449ECB", foreground="navy blue", wraplength=190, justify='left')
    
    closeEdit()
#def editWord(word, newDefinition):
    #dictionary[word] = newDefinition
    
#Add Page variables

#word label
global labelWord
labelWord = tk.Label(root, text="Word", font='Helvetica 18 bold', background="#449ECB", foreground="navy blue")

#word text entry
name = ''
global nameEntryBox
nameEntryBox = tk.Entry(root, textvariable=name, width=26, font="Helvetica 10", background="#A5D9F3", foreground="navy blue")

#definition label
global labelDef
labelDef = tk.Label(root, text="Definition", font='Helvetica 18 bold', background="#449ECB", foreground="navy blue")

#wordLabel
global wordDefLabels
wordDefLabels = {}

#definition entry Box
global defEntryBox
defEntryBox = tk.Text(root, width=26, height=16, font='Helvetica 10', background="#A5D9F3", foreground="navy blue")

#save Button
global saveButton
saveButton = tk.Button(root,
                       text = "Save Word",
                       fg = "black",
                       bg = "green",
                       font = "Helvetica 10",
                       command=saveAWord)

#close Button
global closeAddWordButton
closeAddWordButton = tk.Button(root,
                       text = "Close",
                       fg = "black",
                       bg = "red",
                       font = "Helvetica 10",
                       command=closeAddWord)
global closeWordPageButton
closeWordPageButton = tk.Button(root,
                       text = "Close",
                       fg = "black",
                       bg = "red",
                       font = "Helvetica 10",
                       command=closeWordPage)

#Main page
global addAWordButton
addAWordButton = tk.Button(root,
                           text = "Add a Word",
                           fg = "white",
                           bg = "green",
                           font = "Helvetica 14",
                           command=openAddAWord)
#word page
global currentWord
global currentDefinition
global nextWord
global nextDefinition
global prevWord
global prevDefinition
global deleteButton
deleteButton = tk.Button(root,
                       text = "Delete",
                       fg = "white",
                       bg = "navy",
                       font = "Helvetica 10",
                       command=lambda: deleteWord(currentWord))

global editButton
editButton = tk.Button(root,
                       text = "Edit",
                       fg = "black",
                       bg = "green",
                       font = "Helvetica 10",
                       command=showEditPage)

global cancelButton
cancelButton = tk.Button(root,
                       text = "Cancel",
                       fg = "black",
                       bg = "red",
                       font = "Helvetica 10",
                       command=closeEdit)
global saveEditButton
saveEditButton = tk.Button(root,
                       text = "Save",
                       fg = "black",
                       bg = "green",
                       font = "Helvetica 10",
                       command=saveEdit)
global nextWordButton
nextWordButton = tk.Button(root,
                       text = ">>",
                       fg = "navy",
                       bg = "#A5D9F3",
                       font = "Helvetica 10",
                       command=lambda: nextPrevWordPage(nextWord, nextDefinition))
global prevWordButton
prevWordButton = tk.Button(root,
                       text = "<<",
                       fg = "navy",
                       bg = "#A5D9F3",
                       font = "Helvetica 10",
                       command=lambda: nextPrevWordPage(prevWord, prevDefinition))
for line in lines:
    if (line != '\n'):
        array = line.split(',')
        dictionary[array[0]] = array[1]

drawWords()
root.configure(background="#449ECB")
tk.mainloop()

